import React from 'react';
import {Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// check if the data was successfully passed
	// console.log(props);
	// console.log(typeof props);

	// deconstruct the courseProp into their own variables
	const {_id, name, description, price} = courseProp;
	// let's create our useState hook to store its state
	// States are used to keep track of information related to individual components
	/*Syntax:
		const [currentValue(getter), updateValue(setter)] = useState(InitialGetterValue)
	*/

	// const [count, setCount] = useState(0);
	// const [seat, deductSeat] = useState(30);

	// statehook that indicates availability of course for enrollment (enroll button)
	// const [isOpen, setIsOpen] = useState(true);

	// console.log(count);
	// console.log(seat);

	// added code for available seats
	// const enroll = () => {
	// 	setCount(count + 1);
	// 	deductSeat(seat - 1);

		// if(seat === 0){
		// 	setCount(count);
		// 	deductSeat(seat);
		// 	alert("No Seats Available");
		// }

	// 	console.log("Enrollees" + count);
	// 	console.log("Seats" + seat);
	// };

	// when you call useEffect(), you're telling React to run your "effect" function after flushing changes to the DOM. Effecrs are declared inside the component so they have access to its props and states
	// useEffect(() => {
	// 	if(seat === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [seat]); // it controls the rendering of the useEffect

	return(
			<Card className="cardCourse p-3">
				<Card.Title className="mb-auto p-3">
					<h2>{name}</h2>
				</Card.Title>
				<Card.Body>
					<h4>Description:</h4>
					<p>{description}</p>
					<h4>Price:</h4>
					<p>PhP {price}</p>
					<Link className="btn btn-primary" to={`/courses/${_id}`}>View Course</Link>

				{/*	{isOpen ?
						<Button variant="primary" onClick={enroll}>Enroll</Button>
						:

						<Button variant="primary" disabled>Enroll</Button>
					}*/}
				</Card.Body>
			</Card>
		)
}

// check if the CourseCard component is getting the correct prop types
// PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed down from one component to the next
CourseCard.propTypes ={
	// shape() method => it is used to check if a prop object conforms to a 'specific' shape
	courseProp: PropTypes.shape({
		// define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}