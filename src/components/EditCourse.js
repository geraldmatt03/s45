import React, {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditCourse({course, fetchData}){
	// state for courseId for the fetch URL
	const [courseId, setCourseId] = useState('');

	// forms state
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	// add for the editCourse modal to open and close
	const [showEdit, setShowEdit] = useState(false);

	// function that handles the closing and opening of modals
	const openEdit = (courseId) => {
		// to still get the actual data from the form
		fetch(`https://stormy-everglades-76140.herokuapp.com/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			// populate all the input values with course info that we fetched
			setCourseId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

		// then open the modal
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
	};

	// function to update the course
	const editCourse = (e) => {
		e.preventDefault();

		fetch(`https://stormy-everglades-76140.herokuapp.com/courses/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfully Updated'
				})

				closeEdit();
				fetchData();
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please Try Again'
				})

				closeEdit();
				fetchData();
			}
		})
	};

	return(
		<>
			<Button variant="primary" size="sm"onClick={() => openEdit(course)}>Update</Button>

			{/*Add modal forms*/}
				<Modal show={showEdit} onHide={closeEdit}>
					<Form onSubmit={e => editCourse(e, courseId)}>
						<Modal.Header closeButton>
							<Modal.Title>Edit Course</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<Form.Group>
								<Form.Label>Name</Form.Label>
								<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
							</Form.Group>

							<Form.Group>
								<Form.Label>Description</Form.Label>
								<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
							</Form.Group>

							<Form.Group>
								<Form.Label>Price</Form.Label>
								<Form.Control type="Number" value={price} onChange={e => setPrice(e.target.value)} required />
							</Form.Group>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={closeEdit}>Close</Button>
							<Button variant="success" type="submit">Submit</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		</>
		)
}