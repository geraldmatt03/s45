import React, {useState} from 'react';
import {Button, Form, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddCourse({fetchData}){
	// add state for the forms of course
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	// states for our modals to open/close the modal
	const [showAdd, setShowAdd] = useState(false);

	// function that handles the closing and opening of modals
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	// function for fetching the create course in the backend
	const addCourse = (e) => {
		e.preventDefault();

		fetch('https://stormy-everglades-76140.herokuapp.com/courses/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// validation
			if(data){
				// Show a sucess message
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfully Added'
				})

				// close modal
				closeAdd();
				// render the updated data using the fetchData prop
				fetchData();
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Something Went Wrong. Please Try Again'
				})
				closeAdd();
				fetchData();
			}

			// clear out input fields
			setName('');
			setDescription('');
			setPrice('');
		})
	}

	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Course</Button>

		{/*Add modal forms*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addCourse(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="Number" value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		)
}