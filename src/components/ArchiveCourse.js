import React from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, isActive, fetchData}){
	const archiveToggle = (courseId) => {
		fetch(`https://stormy-everglades-76140.herokuapp.com/courses/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfully Disabled'
				})

				fetchData()
			} else{
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try Again'
				})

				fetchData()
			}
		})
	}

	const unarchiveToggle = (courseId) => {
		fetch(`https://stormy-everglades-76140.herokuapp.com/courses/${courseId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfully Reactivated'
				})

				fetchData()
			} else{
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try Again'
				})

				fetchData()
			}
		})
	}

	return(
		<>
			{isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => unarchiveToggle(course)}>Unarchive</Button>
			}
		</>
		)
}