import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Learn From Home</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Non sequi debitis quas voluptas repellat magni obcaecati. Dolores, soluta debitis maiores eum delectus molestiae sint voluptatibus nihil quas voluptas odio, minima.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
					
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Letter</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Non sequi debitis quas voluptas repellat magni obcaecati. Dolores, soluta debitis maiores eum delectus molestiae sint voluptatibus nihil quas voluptas odio, minima.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Non sequi debitis quas voluptas repellat magni obcaecati. Dolores, soluta debitis maiores eum delectus molestiae sint voluptatibus nihil quas voluptas odio, minima.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}