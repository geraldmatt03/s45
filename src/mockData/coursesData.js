const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam facere, at, aperiam perferendis impedit ratione enim aut veritatis sapiente neque ullam, quasi. Nam praesentium alias accusamus, omnis iste dolorum repellendus.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam facere, at, aperiam perferendis impedit ratione enim aut veritatis sapiente neque ullam, quasi. Nam praesentium alias accusamus, omnis iste dolorum repellendus.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam facere, at, aperiam perferendis impedit ratione enim aut veritatis sapiente neque ullam, quasi. Nam praesentium alias accusamus, omnis iste dolorum repellendus.",
		price: 55000,
		onOffer: true
	}
]
export default coursesData;