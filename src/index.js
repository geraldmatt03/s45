import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


// React.js does not like rendering two adjacent elements. Instead the adjacent elements must be wrapped by a parent element or React Fragmemts (<>....</>)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// JSX -> it is a syntax used in React.js
// JavaScript  + XML -> it is an extension of JS that let's us create objects which will then be compiled and added as HTML elements

// With JSX, we are able to create HTML elements using JS

// index.js is the main entry point. it is importing app, binding it to root in index.html