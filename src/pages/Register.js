import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from	'react-router-dom';


export default function Register(){
	const {user} = useContext(UserContext);

	// state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password2, setPassword2] = useState('');

	// state to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {
		// add validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && gender !== '' && mobileNo !== '') && (password1 === password2)){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2, gender, mobileNo]);

	const registerUser = (e) => {
		// prevents page redirection via form submissions
		e.preventDefault();

		// clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');

		// Swal.fire({
		// 	tittle: "Yaaaaaayy!",
		// 	icon: "success",
		// 	text: "You have successfully registered"
		// });

		fetch(' https://stormy-everglades-76140.herokuapp.com/users/register', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				gender: gender,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setFirstName('');
			setLastName('');
			setEmail('');
			setPassword1('');
			setPassword2('');
			setGender('');
			setMobileNo('');

			Swal.fire({
				title: "Yaaaaaayy!",
				icon: "success",
				text: "You have successfully registered. Please Login to Continue Transaction."
			})

			navigate('/login');
		})
	}


	return(
		// conditional rendering
		(user.accessToken !== null) ?

		<Navigate to="/login" />

		:

		<Form onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="ex. Juan"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="ex. Dela Cruz"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
				<Form.Text className="text-muted">We'll never share your Email with anyone else</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Gender</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Please type gender (Male/Female)"
					required
					value={gender}
					onChange={e => setGender(e.target.value)}
					/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Mobile Number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					/>
			</Form.Group>


			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Register</Button>
				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Register</Button>
			}
		</Form>
	)
};