import React, {useState, useEffect, useContext} from 'react';
import {Container, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function SpecificCourse(){
	// useParams() contains any values we are trying to pass in the URL stored
	// useParams is how we recive the courseId passed via the URL
	const {courseId} = useParams();
	const navigate = useNavigate();

	useEffect(() => {
		fetch(`https://stormy-everglades-76140.herokuapp.com/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])

	const {user} = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	// enroll function
	const enroll = (courseId) => {
		fetch('https://stormy-everglades-76140.herokuapp.com/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: 'Successfully Enrolled',
					icon: 'success', 
				})

				navigate('/courses')
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error'
				})
			}
		})
	};

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>{price}: Php</h6>
				</Card.Body>

				<Card.Footer>
					{user.accessToken !== null ?
						<div className="d-grid gap-2">
						<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
						</div>

						:

						<Link className="btn btn-warning d-grid gap-2" to='/login'>Login to Enroll</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
		)
}