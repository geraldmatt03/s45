import React, {useState, useEffect, useContext} from 'react';
// useContext is used to unpack or deconstruct the value of the UserContext
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Login() {
	// it allows us to consume the UserContext object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(true);
	const navigate = useNavigate();

	useEffect(() =>{
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password]);

	// Notes: fetch() is a method that we use in js which allows to send a request to an API and process its response

		// SYNTAX: fetch('url', {optional}).then(response => response.json()).then(data => {console.log(data)})

			// url => coming from the API/server
			// {optional} => it contains additional information about our requests such as method, body, and headers (Content-Type: application/json) or any other info.
			// .then(response => response.json()) => parse the response as JSON
			// .then(data => {console.log(data)}) => process the actual data

	const loginUser = (e) => {
		e.preventDefault();

		// set the email of the authenticated user in the localStorage
		// localStorage.setItem('property', value)

		// localStorage.setItem('email', email);

		// set the global user state to have properties obtained from the localStorage
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		// setEmail('');
		// setPassword('');

		// Swal.fire({
		// 	title: "Sucess!",
		// 	icon: "success",
		// 	text: `Welcome Back ${email}`
		// });

		// with fetch() => front-end to back-end connection
		fetch(' https://stormy-everglades-76140.herokuapp.com/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			// validation
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken});
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `Welcome Back ${email}`
				});

				// getting the user's credentials 
				fetch(' https://stormy-everglades-76140.herokuapp.com/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result);

					// validation if admin
					if(result.isAdmin === true){
						localStorage.setItem('email', result.email);
						localStorage.setItem('isAdmin', result.isAdmin);
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						// redirect the admin to /courses
						navigate('/courses');
					} else {
						// if not an admin
						navigate('/');
					}
				})

				} else if(data.message === 'User Not Found'){
					Swal.fire({
						title: "Oooops!",
						icon: "error",
						text: "Email Not Registered"
					})
				} else {
					Swal.fire({
						title: "Oooops!",
						icon: "error",
						text: "Something is Wrong. Check Credentials."
					})
				}

				setEmail('');
				setPassword('');
		});
	}

	return(
		// create a conditional rendering statement that will redirect the user to the courses page when a user is logged in

		(user.accessToken !== null) ?

		<Navigate to="/courses" />
		
		:

		<Form onSubmit={(e) => loginUser(e)}>
			<h1>Login User</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Login</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Login</Button>
			}
		</Form>
		)
}